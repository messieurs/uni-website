module.exports = {
    pwa: {
      name: 'Studio web UNI',
      themeColor: '#FF6720',
      msTileColor: '#FFFFFF'
    },

    baseUrl: undefined,
    outputDir: undefined,
    assetsDir: 'assets',
    runtimeCompiler: undefined,
    productionSourceMap: undefined,
    parallel: undefined,

    css: {
          loaderOptions: {
              sass: {
                  data: `@import "@/assets/styles/variables.scss";`
              }
          }
      },

    lintOnSave: false
}
